const fs = require('fs');
const dom = require('xmldom').DOMParser;
const xpath = require('xpath');

const data = [
    { date: '1801', matrix: require('../../data/json/census_1801.json') },
    { date: '1849', matrix: require('../../data/json/census_1849.json') },
    { date: '1864', matrix: require('../../data/json/census_1864.json') },
    { date: '1878', matrix: require('../../data/json/census_1878.json') }
]

var xmlDoc = null;

module.exports = {
    init: function() {
        var data = fs.readFileSync('data/xml/census.xml', { encoding : 'UTF-8' });
        xmlDoc = new dom().parseFromString(data);
        return this;
    },

    censusMatrix: function(date) {
        return data.filter((census) => census.date === date)[0].matrix;
    },
    censusValue: function(date, place, row, col) {
        var matrix = censusMatrix(date);
        var idx = getIdx(date, place);
        return xpath.evaluate("//Workbook/Worksheet[@Name='" + date + "']/Table/R[" + (idx + row) + "]/C[" + matrix[row].offset[col].index + "]/text()", xmlDoc, null, xpath.XPathResult.STRING_TYPE, null).stringValue;
    }
}

function getIdx(date, place) {
    if (xmlDoc != null) {
        return xpath.evaluate("count(//Workbook/Worksheet[@Name='" + date + "']/Table/R[C/text() = '" + place + "']/preceding::R[../../@Name='" + date + "'])+1", xmlDoc, null, xpath.XPathResult.NUMBER_TYPE, null).numberValue;
    }
    return 0;
}
