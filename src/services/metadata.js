const fs = require('fs');
const dom = require('xmldom').DOMParser;
const xpath = require('xpath');

var xmlDoc = null;

module.exports = {
    init: function() {
        var data = fs.readFileSync('data/xml/metadata.xml', { encoding : 'UTF-8' });
        xmlDoc = new dom().parseFromString(data);
        return this;
    },

    totalOfBirthBooks: function(id) { return numberOfBooks(id, "Batismo"); },
    totalOfWeddingBooks: function(id) { return numberOfBooks(id, "Casamento"); },
    totalOfDeathBooks: function(id) { return numberOfBooks(id, "Obito"); },

    completedBirthBooks: function(id) { return numberOfCompletedBooks(id, "Batismo"); },
    completedWeddingBooks: function(id) { return numberOfCompletedBooks(id, "Casamento"); },
    completedDeathBooks: function(id) { return numberOfCompletedBooks(id, "Obito"); },

    fullNumberOfBirthItems: function(id) { return fullNumberOfItems(id, "Batismo"); },
    fullNumberOfWeddingItems: function(id) { return fullNumberOfItems(id, "Casamento"); },
    fullNumberOfDeathItems: function(id) { return fullNumberOfItems(id, "Obito"); }
}

function numberOfBooks(id, kind) {
	if (xmlDoc != null) {
		var xpathExpression = "//metadata/parish[@id='" + id + "']/books[@kind='" + kind + "']/@total";
		return xpath.evaluate(xpathExpression, xmlDoc, null, xpath.XPathResult.NUMBER_TYPE, null).numberValue;
	}
	return 0;
}

function numberOfCompletedBooks(id, kind) {
	if (xmlDoc != null) {
		var xpathExpression = "//metadata/parish[@id='" + id + "']/books[@kind='" + kind + "']/@completed";
		return xpath.evaluate(xpathExpression, xmlDoc, null, xpath.XPathResult.NUMBER_TYPE, null).numberValue;
	}
	return 0;
}

function fullNumberOfItems(id, kind) {
	if (xmlDoc != null) {
		var xpathExpression = "//metadata/parish[@id='" + id + "']/books[@kind='" + kind + "']/@items";
        return xpath.evaluate(xpathExpression, xmlDoc, null, xpath.XPathResult.NUMBER_TYPE, null).numberValue;
	}
	return 0;
}
