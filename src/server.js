const express = require('express');
const url = require('url');
const bodyParser = require('body-parser');

const census = require('./services/census.js').init();
const metadata = require('./services/metadata.js').init();
const parishes = require('./services/parishes.js');

const app = express();
const router = express.Router();

const port = process.env.PORT || 3000;
const originWhiteList = ['http://locahost:3000','https://example.net'];

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//app.get('/', (request, response) => response.send('Hello world'));

router.use((request, response, next) => {
  console.log('Server info: request received');

  let origin = request.headers.origin;
  if (originWhiteList.indexOf(origin) > -1) {
    response.setHeader('Access-Control-Origin', origin);
  }
  response.setHeader('Access-Control-Allow-Methods', 'GET');
  response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  response.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

router.get('/', (request, response) => {
  response.json({message: 'Hello, welcome to my server'});
});

router.get('/parishes', (request, response) => {
  response.json(parishes.getParishes());
});

/**
 * Metadata services
 */
router.get('/metadata/totalOfBirthBooks', (request, response) => {
  var params = url.parse(request.url, true).query;
  response.json(metadata.totalOfBirthBooks(params.id));
});

router.get('/metadata/totalOfWeddingBooks', (request, response) => {
  var params = url.parse(request.url, true).query;
  response.json(metadata.totalOfWeddingBooks(params.id));
});

router.get('/metadata/totalOfDeathBooks', (request, response) => {
  var params = url.parse(request.url, true).query;
  response.json(metadata.totalOfDeathBooks(params.id));
});

router.get('/metadata/completedBirthBooks', (request, response) => {
  var params = url.parse(request.url, true).query;
  response.json(metadata.completedBirthBooks(params.id));
});

router.get('/metadata/completedWeddingBooks', (request, response) => {
  var params = url.parse(request.url, true).query;
  response.json(metadata.completedWeddingBooks(params.id));
});

router.get('/metadata/completedDeathBooks', (request, response) => {
  var params = url.parse(request.url, true).query;
  response.json(metadata.completedDeathBooks(params.id));
});

router.get('/metadata/fullNumberOfBirthItems', (request, response) => {
  var params = url.parse(request.url, true).query;
  response.json(metadata.fullNumberOfBirthItems(params.id));
});

router.get('/metadata/fullNumberOfWeddingItems', (request, response) => {
  var params = url.parse(request.url, true).query;
  response.json(metadata.fullNumberOfWeddingItems(params.id));
});

router.get('/metadata/fullNumberOfDeathItems', (request, response) => {
  var params = url.parse(request.url, true).query;
  response.json(metadata.fullNumberOfDeathItems(params.id));
});

/**
 * Census services
 */
router.get('/census/matrix', (request, response) => {
  var params = url.parse(request.url, true).query;
  response.json(census.censusMatrix(params.date));
});

router.get('/census/value', (request, response) => {
  var params = url.parse(request.url, true).query;
  response.json(census.censusValue(params.date, params.place, parseInt(params.row, 10), parseInt(params.col, 10)));
});

app.use('/api', router);
app.listen(port, () => console.log(`Listening on port ${port}`));
